<?php

namespace Drupal\heading_highlighter;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Heading Highlighter manager.
 */
class HeadingHighlighter implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * Heading highlighter configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Admin context service.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * HeadingHighlighter constructor.
   *
   * @param Drupal\Core\Config\ConfigFactory $config
   *   The configuration manager.
   * @param Drupal\Core\Routing\AdminContext $admin_context
   *   The admin context service.
   */
  public function __construct(ConfigFactory $config, AdminContext $admin_context) {
    $this->config = $config->get('heading_highlighter.settings');
    $this->adminContext = $admin_context;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $config = $container->get('config.factory');
    $admin_context = $container->get('router.admin_context');
    return new static($config, $admin_context);
  }

  /**
   * Attach heading highlighter to page.
   */
  public function attach(&$attachments) {
    if (!$this->access()) {
      return;
    }

    $route = \Drupal::routeMatch()->getRouteObject();
    // Do not load on admin pages.
    if ($this->adminContext->isAdminRoute($route)) {
      return;
    }

    $attachments['#attached']['library'][] = 'heading_highlighter/functionality';
    if ($this->config->get('default_styling')) {
      $attachments['#attached']['library'][] = 'heading_highlighter/styling';
    }

    $container_selector = 'body';
    if ($this->config->get('container') === 'selector') {
      $container_selector = $this->config->get('container_selector');
    }
    elseif ($this->config->get('container') === 'region') {
      $container_selector = '.region.region-' . $this->config->get('container_region');
    }

    // Only allow highlight off if the toggle is visible.
    $highlight_on = !($this->config->get('show_toggle') && !$this->config->get('highlight_default'));

    if ($this->config->get('toggle_text_option')) {
      $toggle_text = $this->config->get('toggle_text_custom');
    }
    else {
      $toggle_text = $this->t('Toggle Heading Highlighter');
    }

    $attachments['#attached']['drupalSettings']['headingHighlighter'] = [
      'selector' => $container_selector,
      'showToggle' => $this->config->get('show_toggle'),
      'togglePlacement' => $this->config->get('toggle_placement'),
      'toggleText' => $toggle_text,
      'highlightDefault' => $highlight_on,
    ];
  }

  /**
   * Handles view permissions for heading highlighter.
   */
  public function access() {
    $account = \Drupal::currentUser();
    $has_permission = AccessResult::allowedIfHasPermission($account, 'view heading_highlighter');
    return !$has_permission->isNeutral();
  }

}
