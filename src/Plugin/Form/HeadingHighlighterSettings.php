<?php

namespace Drupal\heading_highlighter\Plugin\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Heading Highlighter setting block.
 */
class HeadingHighlighterSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'heading_highlighter_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['heading_highlighter.settings'];
  }

  /**
   * Manage translated default text for toggle button.
   */
  protected function defaultToggleText() {
    return $this->t('Toggle Heading Highlighter');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('heading_highlighter.settings');
    $default = $config->get();

    $form['show_toggle'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show toggle'),
      '#default_value' => $default['show_toggle'],
      '#description' => $this->t('Whether or not to show the heading highlighter toggle button.'),
    ];

    $show_toggle_visibility = [
      'visible' => [
        ':input[name="show_toggle"]' => ['checked' => TRUE],
      ],
      'required' => [
        ':input[name="show_toggle"]' => ['checked' => TRUE],
      ],
    ];

    $form['toggle_placement'] = [
      '#title' => $this->t('Toggle placement'),
      '#description' => $this->t('Where the toggle button should appear on the page.'),
      '#type' => 'select',
      '#options' => [
        'bottomLeft' => $this->t('Bottom left'),
        'bottomCenter' => $this->t('Bottom center'),
        'bottomRight' => $this->t('Bottom right'),
        'topLeft' => $this->t('Top left'),
        'topCenter' => $this->t('Top center'),
        'topRight' => $this->t('Top right'),
        'midLeft' => $this->t('Mid left'),
        'midRight' => $this->t('Mid right'),
      ],
      '#default_value' => $default['toggle_placement'],
      '#states' => $show_toggle_visibility,
    ];

    $form['toggle_text_option'] = [
      '#title' => $this->t('Toggle text option'),
      '#type' => 'select',
      '#options' => [
        0 => $this->t('Default') . ': ' . $this->defaultToggleText(),
        1 => $this->t('Custom text'),
      ],
      '#default_value' => $default['toggle_text_option'],
      '#states' => $show_toggle_visibility,
    ];

    $form['toggle_text_custom'] = [
      '#title' => $this->t('Custom toggle text'),
      '#type' => 'textfield',
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $default['toggle_text_custom'],
      '#states' => [
        'visible' => [
          ':input[name="show_toggle"]' => ['checked' => TRUE],
          ':input[name="toggle_text_option"]' => ['value' => 1],
        ],
        'required' => [
          ':input[name="show_toggle"]' => ['checked' => TRUE],
          ':input[name="toggle_text_option"]' => ['value' => 1],
        ],
      ],
    ];

    $form['highlight_default'] = [
      '#type' => 'radios',
      '#title' => $this->t('Highlight by default'),
      '#default_value' => $default['highlight_default'] ? 1 : 0,
      '#options' => [
        0 => $this->t('Disable highlight until toggled on'),
        1 => $this->t('Highlight until toggled off'),
      ],
      '#states' => $show_toggle_visibility,
    ];

    $form['default_styling'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use default styling'),
      '#default_value' => $default['default_styling'],
      '#description' => $this->t('Use the default CSS styles that come with the module.'),
    ];

    $form['container'] = [
      '#type' => 'radios',
      '#title' => $this->t('Contain highlighter to part of the page'),
      '#default_value' => $default['container'],
      '#options' => [
        'none' => $this->t('No container, highlight all page headings'),
        'region' => $this->t('Highlight specific region'),
        'selector' => $this->t('Highlight within CSS selector'),
      ],
    ];

    $container_input = ':input[name="container"]';

    $theme = $this->configFactory->get('system.theme')->get('default');
    $regions = system_region_list($theme);

    $form['container_region'] = [
      '#type' => 'select',
      '#title' => 'Select container region',
      '#default_value' => $default['container_region'],
      '#options' => $regions,
      '#states' => [
        'visible' => [
          $container_input => ['value' => 'region'],
        ],
        'required' => [
          $container_input => ['value' => 'region'],
        ],
      ],
    ];

    $form['container_selector'] = [
      '#type' => 'textfield',
      '#title' => 'Enter CSS selector for the container',
      '#default_value' => $default['container_selector'],
      '#size' => 60,
      '#maxlength' => 128,
      '#states' => [
        'visible' => [
          $container_input => ['value' => 'selector'],
        ],
        'required' => [
          $container_input => ['value' => 'selector'],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('heading_highlighter.settings');
    $form_values = $form_state->getValues();
    $config->set('show_toggle', $form_values['show_toggle'])
      ->set('toggle_placement', $form_values['toggle_placement'])
      ->set('toggle_text_option', $form_values['toggle_text_option'])
      ->set('toggle_text_custom', $form_values['toggle_text_custom'])
      ->set('highlight_default', $form_values['highlight_default'])
      ->set('default_styling', $form_values['default_styling'])
      ->set('container', $form_values['container'])
      ->set('container_region', $form_values['container_region'])
      ->set('container_selector', $form_values['container_selector'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
