(function (Drupal, once, drupalSettings) {
  'use strict';

  class HeadingHighlighter {
    selector;
    highlightDefault;
    showToggle;
    togglePlacement;
    toggleText;
    markers = [];

    /**
     * Construct a HeadingHighlighter object.
     */
    constructor({
      selector = 'body',
      highlightDefault = true,
      showToggle = false,
      togglePlacement = '',
      toggleText = '',
    }) {
      this.selector = selector;
      this.highlightDefault = highlightDefault;
      this.showToggle = showToggle;
      this.togglePlacement = togglePlacement;
      this.toggleText = toggleText;

      this.init();
    }

    init() {
      // Check for heading container.
      const containers = once('heading-highlighter-init', this.selector);
      if (!containers.length) {
        console.warn(
          `Heading highlighter could not find container element matching: ${this.selector}`
        );
        return;
      }

      // Process containers.
      containers.forEach((container) => {
        // Query for heading elements inside container.
        const headingElements = [
          ...container.querySelectorAll('h1, h2, h3, h4, h5, h6'),
        ];
        if (!headingElements.length) return;

        // Generate heading markers and append to list.
        this.markers = this.markers.concat(
          this.buildHeadingMarkers(headingElements)
        );
      });

      if (!this.markers.length) {
        console.warn(
          `Heading highlighter could not find heading elements inside: ${this.selector}`
        );
        return;
      }

      this.buildToggle();
    }

    buildHeadingMarkers(headingElements) {
      return headingElements.map((heading) => {
        const tagName = heading.tagName.toLowerCase();
        const marker = document.createElement('span');
        marker.classList = [
          'heading-highlighter__heading',
          'heading-highlighter__heading--' + tagName,
        ].join(' ');
        marker.textContent = tagName.toUpperCase();
        if (!this.highlightDefault) {
          marker.style.display = 'none';
        }
        heading.prepend(marker);
        return marker;
      });
    }

    buildToggle() {
      if (!this.showToggle) {
        return;
      }
      once('heading-highlighter-toggle-init', 'body').forEach((body) => {
        // Build toggle button.
        const button = document.createElement('button');
        button.classList.add('heading-highlighter-toggle');
        button.classList.add(this.togglePlacement);
        const toggleStyles = [];
        button.setAttribute('style', toggleStyles.join('; '));
        button.append(this.toggleText);

        // Append toggle button to page.
        body.append(button);

        // Add toggle action.
        button.addEventListener('click', (e) => {
          this.toggleAllMarkers();
        });
      });
    }

    toggleAllMarkers() {
      this.markers.forEach((marker) => this.toggleDisplay(marker));
    }

    toggleDisplay(element) {
      if (element.style.display === 'none') {
        element.style.display = '';
      } else {
        element.style.display = 'none';
      }
    }
  }

  Drupal.behaviors.heading_highlighter = {
    attach: function (context, settings) {
      if (!settings.headingHighlighter) return;

      new HeadingHighlighter(settings.headingHighlighter);
    },
    detach: function (context, settings) {},
  };
})(Drupal, once, drupalSettings);
