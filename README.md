# Heading Highlighter

It is [important for web accessibility](https://www.w3.org/WAI/tutorials/page-structure/headings/) to keep a consistent heading hierarchy without skipping levels. This can be difficult to maintain when it is not visually apparent to developers and content editors which text is using which heading element. This module prepends headings on the page with the name of the respective heading level, so that skips are more noticeable and page hierarchies can be created more intentionally.

## Features

- Configure permissions for which users see highlighted headings.
- Set containers for highlighting.
  - For example, only highlight headings inside the main content region.
- Optionally add a toggle button to the page so users can turn highlighting on and off

## Post-installation

The module works out of the box for administrators. You can allow other users to view the highlighting in the permissions page. Specific configuration for the module can be done at `/admin/config/user-interface/heading-highlighter`.
